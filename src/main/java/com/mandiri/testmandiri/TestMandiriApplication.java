package com.mandiri.testmandiri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestMandiriApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestMandiriApplication.class, args);
	}

}
