package com.mandiri.testmandiri.controller;
import com.mandiri.testmandiri.entity.Product;
import com.mandiri.testmandiri.service.ProductService;
import com.mandiri.testmandiri.util.WebResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseEntity<WebResponse<Product>> createProduct(@RequestBody Product product){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new WebResponse<>("Product created successfully",
                        productService.createNew(product)));
    }

    @GetMapping
    public List<Product> getAllProduct(){
        return productService.getAll();
    }

    @PutMapping
    public ResponseEntity<WebResponse<Product>> updateProduct(@RequestBody Product product){
        return ResponseEntity.status(HttpStatus.OK)
                .body(new WebResponse<>("Product updated successfully",
                        productService.update(product)));
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<WebResponse<Void>> deleteProduct(@PathVariable("productId") String id){
        return ResponseEntity.status(HttpStatus.OK)
                .body(new WebResponse<>(String.format("Product deleted successfully"),
                        productService.delete(id)));
    }
}
