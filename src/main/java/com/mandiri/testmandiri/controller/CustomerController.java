package com.mandiri.testmandiri.controller;
import com.mandiri.testmandiri.entity.Customer;
import com.mandiri.testmandiri.service.CustomerService;
import com.mandiri.testmandiri.util.WebResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping
    public ResponseEntity<WebResponse<Customer>> createCustomer(@RequestBody Customer customer){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new WebResponse<>("Customer created successfully",
                        customerService.createNew(customer)));
    }

    @GetMapping
    public ResponseEntity<WebResponse<List<Customer>>> getAllCustomer() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new WebResponse<>("List Customer",
                        customerService.getAll()));
    }

    @PutMapping
    public ResponseEntity<WebResponse<Customer>> updateCustomer(@RequestBody Customer customer){
        return ResponseEntity.status(HttpStatus.OK)
                .body(new WebResponse<>("Customer updated successfully",
                        customerService.update(customer)));
    }

    @DeleteMapping("/{customerId}")
    public ResponseEntity<WebResponse<Void>> deleteCustomer(@PathVariable("customerId") String id){
        return ResponseEntity.status(HttpStatus.OK)
                .body(new WebResponse<>("Customer deleted",
                        customerService.delete(id)));
    }
}
