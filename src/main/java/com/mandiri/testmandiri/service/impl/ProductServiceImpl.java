package com.mandiri.testmandiri.service.impl;
import com.mandiri.testmandiri.entity.Product;
import com.mandiri.testmandiri.repository.ProductRepository;
import com.mandiri.testmandiri.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public Product createNew(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product update(Product product) {
        Product productById = getById(product.getId());
        if (product.getName() != null){
            productById.setName(product.getName());
        }
        if (product.getStock() != null){
            productById.setStock(product.getStock());
        }
        if (product.getPrice() != null){
            productById.setPrice(product.getPrice());
        }
        return productRepository.save(productById);
    }

    @Override
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public Void delete(String id) {
        productRepository.deleteById(id);
        return null;
    }

    public Product getById(String id) {
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()){
            return product.get();
        }
        else{
            throw new RuntimeException("Can't Find Product");
        }
    }
}
