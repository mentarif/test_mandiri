package com.mandiri.testmandiri.service.impl;
import com.mandiri.testmandiri.entity.Customer;
import com.mandiri.testmandiri.repository.CustomerRepository;
import com.mandiri.testmandiri.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository custRepository;

    @Override
    public Customer createNew(Customer customer) {
        return custRepository.save(customer);
    }

    @Override
    public Customer update(Customer customer) {
        Customer customerById = getById(customer.getId());
        if (customer.getName() != null){
            customerById.setName(customer.getName());
        }
        if (customer.getEmail() != null){
            customerById.setEmail(customer.getEmail());
        }
        if (customer.getPhone() != null){
            customerById.setPhone(customer.getPhone());
        }
        if (customer.getAddress() != null){
            customerById.setAddress(customer.getAddress());
        }
        return custRepository.save(customerById);
    }

    @Override
    public List<Customer> getAll() {
        return custRepository.findAll();
    }

    @Override
    public Void delete(String id) {
        custRepository.deleteById(id);
        return null;
    }

    public Customer getById(String id) {
        Optional<Customer> customer = custRepository.findById(id);
        if (customer.isPresent()){
            return customer.get();
        }
        else{
            throw new RuntimeException("Can't Find Customer");
        }
    }
}
