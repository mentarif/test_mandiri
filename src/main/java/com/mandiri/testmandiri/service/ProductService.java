package com.mandiri.testmandiri.service;
import com.mandiri.testmandiri.entity.Product;
import java.util.List;

public interface ProductService {
    Product createNew(Product product);
    Product update(Product product);
    List<Product> getAll();
    Void delete(String id);


}
