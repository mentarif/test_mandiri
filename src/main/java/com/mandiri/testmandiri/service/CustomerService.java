package com.mandiri.testmandiri.service;
import com.mandiri.testmandiri.entity.Customer;
import java.util.List;

public interface CustomerService {
    Customer createNew(Customer customer);
    Customer update(Customer customer);
    List<Customer> getAll();
    Void delete(String id);
}
